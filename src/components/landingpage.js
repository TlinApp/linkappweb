import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';

import { Table } from 'reactstrap';
import dbConfig from '../config/firebase'
const db = dbConfig.firestore();


class Landing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      priceMatrix: [],
    };
  }

  componentDidMount() {
    db.collection('priceMatrix')
      .onSnapshot((QuerySnapshot) => {
        const resources = [];
        QuerySnapshot.forEach((doc) => {

          const { name, description, location, type, unit, provider, cost, price, status } = doc.data();
          resources.push({
            id: doc.id,
            doc,
            name,
            description,
            location,
            type,
            unit,
            provider,
            cost,
            price,
            status
          });
        });
        this.setState({
          priceMatrix: resources,
          isLoading: false,
        });
      });
  }

  renderResources(res, index) {
    return (
      <tr key={index}>
        <td>{index + 1}</td>
        <td>{res.name}</td>
        <td>{res.description}</td>
        <td>{res.location}</td>
        <td>{res.type}</td>
        <td>{res.unit}</td>
        <td>{res.provider}</td>
        <td>{"$ " + res.cost}</td>
        <td>{"$ " + res.price}</td>
      </tr>
    )
  }

  render() {
    return (
      <div style={{ width: '100%', margin: 'auto' }}>
        <Grid >
          <Cell col={12}>

            <Table >
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Descripción</th>
                  <th>Ubicación</th>
                  <th>Tipo</th>
                  <th>Unidad</th>
                  <th>Proveedor</th>
                  <th>Costo</th>
                  <th>Precio</th>
                </tr>
              </thead>
              <tbody>
                {this.state.priceMatrix.map(this.renderResources)}
              </tbody>
            </Table>

          </Cell>
        </Grid>
      </div>
    )
  }
}

export default Landing;
